$(document).ready(function () {
  'use strict';

  var sliderDiv = $('#sliderDiv').find('div')
  $('#slider-identifier').empty()
  var current = 0
  var final = sliderDiv.length - 1

  for (var index = 0; index < sliderDiv.length; index++) {
    $(sliderDiv[index]).attr('data-slide', index)
    var span = $('<span></span>').attr('data-identifier', index)
    if (index == 0) {
      span.addClass('w-4 h-4  border border-black block active identifier')
    } else {
    $(sliderDiv[index]).attr('data-slide', index).addClass('hidden')
      span.addClass('w-4 h-4  border border-black bg-white block identifier')
    }

    $('#slider-identifier').append(span)
  }

  $('.identifier').on('click', function () {
    var newSlider = this.dataset.identifier
    changeElement(newSlider)
  })

  setInterval(function () {
    if (current == final) {
      changeElement(0)
    } else {
      changeElement((Number(current)+1))
    }
  }, 4000);

  function changeElement(newSlide) {
    if (current != newSlide) {
      $('[data-slide="' + current + '"]').addClass('hidden')
      $('[data-identifier="' + current + '"]').removeClass('active').addClass('bg-white')
      $('[data-slide="' + newSlide + '"]').removeClass('hidden')
      $('[data-identifier="' + newSlide + '"]').removeClass('bg-white').addClass('active')
      current = newSlide
    }
  }

});
